# Airflow
<a href="https://gitmoji.dev">
  <img src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square" alt="Gitmoji">
</a>

## Sobre

Esse projeto é um docker-compose para subir um AirFlow com execução Celery.

Esse projeto se relaciona com [DAGs Repo](https://gitlab.com/pedroivobr/dags_repo) para desenvolvimento das DAGs.

## Usar

Para usar bastar ter o Docker e DockerCompose instalados na máquina, com isso execute para iniciar:

> docker-compose up -d

e para parar:

> docker-compose down